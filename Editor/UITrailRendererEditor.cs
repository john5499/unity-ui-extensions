﻿using UnityEditor;

namespace UnityEngine.UI.Extensions
{
    /// <summary>
    /// Editor class used to edit UI Graphics.
    /// Extend this class to write your own graphic editor.
    /// </summary>

    [CustomEditor(typeof(UITrailRenderer), false)]
    [CanEditMultipleObjects]
    public class UITrailRendererEditor : Editor
    {
        protected SerializedProperty m_Script;
        protected SerializedProperty m_Material;
        protected SerializedProperty m_Sprite;
        protected SerializedProperty m_TrailColor;
        protected SerializedProperty m_Width;
        protected SerializedProperty m_RaycastTarget;

        protected virtual void OnDisable()
        {
            Tools.hidden = false;
        }

        protected virtual void OnEnable()
        {
            m_Script = serializedObject.FindProperty("m_Script");

            m_Material = serializedObject.FindProperty("m_Material");
            m_Sprite = serializedObject.FindProperty("m_Sprite");

            m_TrailColor = serializedObject.FindProperty("m_TrailColor");
            m_Width = serializedObject.FindProperty("m_Width");

            m_RaycastTarget = serializedObject.FindProperty("m_RaycastTarget");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(m_Script);

            AppearanceControlsGUI();
            TrailControlGUI();
            RaycastControlsGUI();

            serializedObject.ApplyModifiedProperties();
        }

        public override bool HasPreviewGUI() { return true; }

        public override void OnPreviewGUI(Rect rect, GUIStyle background)
        {
            UITrailRenderer renderer = target as UITrailRenderer;
            if (renderer == null) return;

            Sprite sf = renderer.sprite;
            if (sf == null) return;

            SpriteDrawUtility.DrawSprite(sf, rect, renderer.canvasRenderer.GetColor());
        }

        protected void TrailControlGUI()
        {
            var renderer = target as UITrailRenderer;

            renderer.time = EditorGUILayout.FloatField("Track point expire time", renderer.time);
            renderer.emitting = EditorGUILayout.Toggle("Emitting track points", renderer.emitting);
            renderer.minVertexDistance = EditorGUILayout.FloatField("Min distance between track point", renderer.minVertexDistance);
            renderer.numCapVertices = EditorGUILayout.IntField("Number of cap vertices", renderer.numCapVertices);

            EditorGUILayout.PropertyField(m_TrailColor);
            EditorGUILayout.CurveField("Trail width", renderer.width, Color.red, new Rect(0.0f, 0.0f, 1.0f, 100.0f));
        }

        protected void AppearanceControlsGUI()
        {
            EditorGUILayout.PropertyField(m_Sprite);
            EditorGUILayout.PropertyField(m_Material);
        }

        /// <summary>
        /// GUI related to the Raycasting settings for the graphic.
        /// </summary>
        protected void RaycastControlsGUI()
        {
            EditorGUILayout.PropertyField(m_RaycastTarget);
        }
    }
}
