﻿using UnityEngine;
using System.Collections.Generic;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample01
{
    public class ScrollView : FancyScrollView<ItemData>
    {
        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;

        protected override GameObject m_CellPrefab => cellPrefab;

        protected override void Start()
        {
            base.Start();
            scroller.onValueChanged += UpdatePosition;
        }

        public void UpdateData(IList<ItemData> items)
        {
            UpdateContents(items);
            scroller.maxPosition = items.Count - 1;
        }

        public Scroller Scroller => scroller;
    }
}
