﻿using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample02
{
    public class Cell : FancyScrollViewCell<ItemData, Context>
    {
        [SerializeField] Animator animator = default;
        [SerializeField] Text message = default;
        [SerializeField] Image image = default;
        [SerializeField] Button button = default;

        static class AnimatorHash
        {
            public static readonly int Scroll = Animator.StringToHash("scroll");
        }

        public override void SetupContext(Context context)
        {
            base.SetupContext(context);
            button.onClick.AddListener(() => m_Context.OnCellClicked?.Invoke(index));
            m_Context.OnSelectionChanged += UpdateSelection;
        }

        private void UpdateSelection()
        {
            if (!isActiveAndEnabled)
                return;

            var selected = m_Context.SelectedIndex == index;
            image.color = selected ? new Color32(0, 255, 255, 100) : new Color32(255, 255, 255, 77);
        }

        public override void UpdateContent(ItemData itemData)
        {
            message.text = itemData.Message;
            UpdateSelection();
        }

        public override void UpdatePosition(float position)
        {
            currentPosition = position;

            if (animator.isActiveAndEnabled)
            {
                animator.Play(AnimatorHash.Scroll, -1, position);
            }

            animator.speed = 0;
        }

        // GameObject が非アクティブになると Animator がリセットされてしまうため
        // 現在位置を保持しておいて OnEnable のタイミングで現在位置を再設定します
        float currentPosition = 0;

        void OnEnable() => UpdatePosition(currentPosition);
    }
}
