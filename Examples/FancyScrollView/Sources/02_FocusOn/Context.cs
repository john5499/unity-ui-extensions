﻿using System;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample02
{
    public class Context
    {
        // SV -> Cell
        public int SelectedIndex = -1;
        // Cell -> SV
        public Action<int> OnCellClicked;
        // SV -> Cell
        public Action OnSelectionChanged;
    }
}
