﻿using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample03
{
    public class Cell : FancyScrollViewCell<ItemData, Context>
    {
        [SerializeField] Animator animator = default;
        [SerializeField] Text message = default;
        [SerializeField] Text messageLarge = default;
        [SerializeField] Image image = default;
        [SerializeField] Image imageLarge = default;
        [SerializeField] Button button = default;

        private bool m_Selected;

        static class AnimatorHash
        {
            public static readonly int Scroll = Animator.StringToHash("scroll");
        }

        void Start()
        {
            m_Selected = false;
            imageLarge.color = image.color = new Color32(255, 255, 255, 77);

            button.onClick.AddListener(() => m_Context.OnCellClicked?.Invoke(index));
        }

        public override void SetupContext(Context context)
        {
            base.SetupContext(context);
            m_Context.OnSelectIndexChanged += UpdateSelection;
        }

        public override void UpdateContent(ItemData itemData)
        {
            // Debug.Log($"Update cell content to data {Index}");
            message.text = itemData.Message;
            messageLarge.text = index.ToString();
            UpdateSelection();
        }

        public override void UpdatePosition(float position)
        {
            currentPosition = position;

            if (animator.isActiveAndEnabled)
            {
                animator.Play(AnimatorHash.Scroll, -1, position);
            }

            animator.speed = 0;
        }

        private void UpdateSelection()
        {
            if (!isActiveAndEnabled)
                return;

            if (!m_Selected && index == m_Context.SelectedIndex)
            {
                // Debug.Log($"Update cell selection state of data {Index}");
                m_Selected = true;
                imageLarge.color = image.color = new Color32(0, 255, 255, 100);
            }
            else if (m_Selected && index != m_Context.SelectedIndex)
            {
                // Debug.Log($"Update cell selection state of data {Index}");
                m_Selected = false;
                imageLarge.color = image.color = new Color32(255, 255, 255, 77);
            }
        }

        // GameObject が非アクティブになると Animator がリセットされてしまうため
        // 現在位置を保持しておいて OnEnable のタイミングで現在位置を再設定します
        float currentPosition = 0;

        void OnEnable() => UpdatePosition(currentPosition);
    }
}
