﻿using System.Collections.Generic;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample03
{
    public class ScrollView : FancyScrollView<ItemData, Context>
    {
        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;

        protected override GameObject m_CellPrefab => cellPrefab;

        protected override void Awake()
        {
            base.Awake();
            m_Context.OnCellClicked = SelectCell;
        }

        protected override void Start()
        {
            base.Start();
            scroller.onValueChanged += UpdatePosition;
            scroller.onSelectionChanged += UpdateSelection;
        }

        void UpdateSelection(int index)
        {
            index = CircularIndex(index, m_ItemsSource.Count);
            if (m_Context.SelectedIndex == index)
            {
                return;
            }

            m_Context.SelectedIndex = index;
            m_Context.OnSelectIndexChanged?.Invoke();
        }

        public void UpdateData(IList<ItemData> items)
        {
            scroller.maxPosition = items.Count - 1;
            UpdateContents(items);
        }

        public void SelectCell(int index)
        {
            if (index < 0 || index >= m_ItemsSource.Count || index == m_Context.SelectedIndex)
            {
                return;
            }

            scroller.ScrollTo(index, 0.35f, Ease.OutCubic);
        }
    }
}
