﻿using System;
using System.Collections.Generic;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample04
{
    public class ScrollView : FancyScrollView<ItemData, Context>
    {
        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;

        Action<int> onSelectionChanged;

        protected override GameObject m_CellPrefab => cellPrefab;

        public int CellInstanceCount => Mathf.CeilToInt(1f / Mathf.Max(m_CellInterval, 1e-3f));

        protected override void Awake()
        {
            base.Awake();
            m_Context.OnCellClicked = SelectCell;
        }

        protected override void Start()
        {
            base.Awake();
            scroller.onValueChanged += UpdatePosition;
            scroller.onSelectionChanged += UpdateSelection;
        }

        void UpdateSelection(int index)
        {
            index = CircularIndex(index, m_ItemsSource.Count);

            if (m_Context.SelectedIndex == index)
                return;

            m_Context.SelectedIndex = index;
            m_Context.OnSelectIndexChanged?.Invoke();
            onSelectionChanged?.Invoke(index);
        }

        public void UpdateData(IList<ItemData> items)
        {
            scroller.maxPosition = items.Count;
            UpdateContents(items);
        }

        public void OnSelectionChanged(Action<int> callback)
        {
            onSelectionChanged = callback;
        }

        public void SelectNextCell()
        {
            SelectCell(m_Context.SelectedIndex + 1);
        }

        public void SelectPrevCell()
        {
            SelectCell(m_Context.SelectedIndex - 1);
        }

        public void SelectCell(int index)
        {
            if (index < 0 || index >= m_ItemsSource.Count || index == m_Context.SelectedIndex)
            {
                return;
            }

            scroller.ScrollTo(index, 0.35f, Ease.InOutCubic);
        }

        public Vector4[] GetCellState()
        {
            m_Context.UpdateCellState?.Invoke();
            return m_Context.CellState;
        }

        public void SetCellState(int cellIndex, int dataIndex, float x, float y, float scale)
        {
            m_Context.SetCellState(cellIndex, dataIndex, x, y, scale);
        }
    }
}
