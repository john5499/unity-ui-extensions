﻿namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample07
{
    public class Cell : FancyScrollRectCell<ItemData, Context>
    {
        [SerializeField] Text message = default;
        [SerializeField] Image image = default;

        public override void UpdateContent(ItemData itemData)
        {
            message.text = itemData.Message;

            var selected = m_Context.SelectedIndex == index;
            image.color = selected
                ? new Color32(0, 255, 255, 100)
                : new Color32(255, 255, 255, 77);
        }

        public override void UpdatePosition(float position)
        {
            base.UpdatePosition(position);

            var x = Mathf.Sin(position * Mathf.PI * 2) * 65;
            // Debug.Log($"Cell UpdatePosition index: {Index}, x: {x}, y: {y}");

            var localPos = transform.localPosition;
            localPos.x = x;
            transform.localPosition = localPos;
        }
    }
}