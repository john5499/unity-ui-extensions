﻿using System;
using System.Collections.Generic;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample07
{
    public class FancyScrollRect : FancyScrollRect<ItemData, Context>
    {
        [SerializeField] GameObject cellPrefab = default;

        protected override GameObject m_CellPrefab => cellPrefab;

        public int DataCount => m_ItemsSource.Count;

        public void UpdateData(IList<ItemData> items)
        {
            UpdateContents(items);
        }

        public void ScrollTo(int index, float duration, Alignment alignment = Alignment.Center)
        {
            UpdateSelection(index);
            ScrollTo(index, duration, Ease.InOutQuint, alignment);
        }

        public void JumpTo(int index, Alignment alignment = Alignment.Center)
        {
            UpdateSelection(index);
            scroller.currentPosition = ScorllViewToScrollerPosition(index, alignment);
        }

        void UpdateSelection(int index)
        {
            if (m_Context.SelectedIndex == index)
            {
                return;
            }

            m_Context.SelectedIndex = index;
            Refresh();
        }
    }
}