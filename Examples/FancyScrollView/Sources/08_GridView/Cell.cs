﻿/*
 * FancyScrollView (https://github.com/setchi/FancyScrollView)
 * Copyright (c) 2019 setchi
 * Licensed under MIT (https://github.com/setchi/FancyScrollView/blob/master/LICENSE)
 */

using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample08
{
    public class Cell : FancyScrollViewCell<ItemData, Context>
    {
        [SerializeField]
        private Text m_Message = default;
        [SerializeField]
        private Image m_Image = default;

        public override void SetupContext(Context context)
        {
            base.SetupContext(context);
            m_Context.OnSelectedItemChanged += UpdateSelection;
        }

        private void UpdateSelection()
        {
            var selected = m_Context.SelectedItemIndex == index;
            m_Image.color = selected ? new Color32(0, 255, 255, 100) : new Color32(255, 255, 255, 77);
        }

        public override void UpdateContent(ItemData itemData)
        {
            m_Message.text = itemData.Index.ToString();
            UpdateSelection();
        }

        public override void UpdatePosition(float position)
        {
            var spacing = m_Context.GetColumnSpacing();
            var columnCount = m_Context.GetColumnCount();
            var count = index % columnCount;

            var cellSize = (transform as RectTransform).rect.width;
            var x = (cellSize + spacing) * (count - (columnCount - 1) * 0.5f);
            var y = transform.localPosition.y;
            var wave = Mathf.Sin(position * Mathf.PI * 2) * 65;

            transform.localPosition = new Vector2(x + wave, y);
        }
    }
}
