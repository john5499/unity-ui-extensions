﻿/*
 * FancyScrollView (https://github.com/setchi/FancyScrollView)
 * Copyright (c) 2019 setchi
 * Licensed under MIT (https://github.com/setchi/FancyScrollView/blob/master/LICENSE)
 */
using System;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample08
{
    public class Context : FancyGridViewContext
    {
        public int SelectedItemIndex = -1;
        public Action OnSelectedItemChanged;
    }
}
