﻿/*
 * FancyScrollView (https://github.com/setchi/FancyScrollView)
 * Copyright (c) 2019 setchi
 * Licensed under MIT (https://github.com/setchi/FancyScrollView/blob/master/LICENSE)
 */

using System;
using System.Linq;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample08
{
    public class Example08 : MonoBehaviour
    {
        [SerializeField] FancyGridView gridView = default;
        [SerializeField] InputField paddingTopInputField = default;
        [SerializeField] InputField paddingBottomInputField = default;
        [SerializeField] InputField xSpacingInputField = default;
        [SerializeField] InputField ySpacingInputField = default;
        [SerializeField] InputField dataCountInputField = default;
        [SerializeField] InputField selectIndexInputField = default;
        [SerializeField] Dropdown alignmentDropdown = default;

        void Start()
        {
            gridView.Initialize();

            paddingTopInputField.onValueChanged.AddListener(_ =>
                TryParseValue(paddingTopInputField, 0, 999, value => gridView.paddingHead = value));
            paddingTopInputField.text = gridView.paddingHead.ToString();

            paddingBottomInputField.onValueChanged.AddListener(_ =>
                TryParseValue(paddingBottomInputField, 0, 999, value => gridView.paddingTail = value));
            paddingBottomInputField.text = gridView.paddingTail.ToString();

            xSpacingInputField.onValueChanged.AddListener(_ =>
                TryParseValue(xSpacingInputField, 0, 99, value => gridView.columnSpacing = value));
            xSpacingInputField.text = gridView.columnSpacing.ToString();

            ySpacingInputField.onValueChanged.AddListener(_ =>
                TryParseValue(ySpacingInputField, 0, 99, value => gridView.spacing = value));
            ySpacingInputField.text = gridView.spacing.ToString();

            alignmentDropdown.AddOptions(Enum.GetNames(typeof(Alignment)).Select(x => new Dropdown.OptionData(x)).ToList());
            alignmentDropdown.onValueChanged.AddListener(_ => SelectCell());
            alignmentDropdown.value = (int)Alignment.Center;

            selectIndexInputField.onValueChanged.AddListener(_ => SelectCell());
            selectIndexInputField.text = "50";

            dataCountInputField.onValueChanged.AddListener(_ =>
                TryParseValue(dataCountInputField, 1, 99999, GenerateCells));
            dataCountInputField.text = "100";

            gridView.JumpTo(50);
        }

        void TryParseValue(InputField inputField, int min, int max, Action<int> success)
        {
            if (!int.TryParse(inputField.text, out int value))
            {
                return;
            }

            if (value < min || value > max)
            {
                inputField.text = Mathf.Clamp(value, min, max).ToString();
                return;
            }

            success(value);
        }

        void SelectCell()
        {
            if (gridView.dataCount <= 0)
            {
                return;
            }

            TryParseValue(selectIndexInputField, 0, gridView.dataCount - 1, index =>
            {
                gridView.UpdateSelection(index);
                gridView.ScrollTo(index, 0.4f, Ease.InOutQuint, (Alignment)alignmentDropdown.value);
            });
        }

        void GenerateCells(int dataCount)
        {
            var items = Enumerable.Range(0, dataCount)
                .Select(i => new ItemData(i))
                .ToArray();

            gridView.UpdateContents(items);
            SelectCell();
        }
    }
}
