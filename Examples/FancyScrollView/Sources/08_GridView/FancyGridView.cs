﻿/*
 * FancyScrollView (https://github.com/setchi/FancyScrollView)
 * Copyright (c) 2019 setchi
 * Licensed under MIT (https://github.com/setchi/FancyScrollView/blob/master/LICENSE)
 */

using UnityEngine;

namespace UnityEngine.UI.Extensions.Examples.FancyScrollViewExample08
{
    public class FancyGridView : FancyGridView<ItemData, Context>
    {
        public void UpdateSelection(int index)
        {
            if (m_Context.SelectedItemIndex == index)
            {
                return;
            }

            m_Context.SelectedItemIndex = index;
            m_Context.OnSelectedItemChanged?.Invoke();
        }
    }
}
