﻿using System;
using System.Collections.Generic;
using UnityEngine.U2D;

namespace UnityEngine.UI.Extensions
{
    public class UITrailRenderer : MaskableGraphic
    {
        /// <summary>
        /// Trail上的点
        /// </summary>
        private struct TrackPoint
        {
            public float ExpireTime;
            public Vector3 Position;
        }

        /// <summary>
        /// 用来帮助构造Mesh的工具
        /// </summary>
        [NonSerialized] 
        private static readonly VertexHelper s_VertexHelper = new VertexHelper();

        [NonSerialized]
        private static bool s_Initialized = false;

        /// <summary>
        /// 用来跟踪还没将Sprite和对应的SpriteAtlas进行绑定的UITrailRenderer
        /// </summary>
        [NonSerialized]
        static List<UITrailRenderer> m_TrackedUITrailRendererList = new List<UITrailRenderer>();

        [SerializeField]
        [Min(0.0f)]
        private float m_Time = 0.5f;
        /// <summary>
        /// 路径上每个点的存活时间
        /// </summary>
        public float time
        {
            get { return m_Time;}
            set
            {
                value = Mathf.Max(0, value);
                if (SetPropertyUtility.SetStruct(ref m_Time, value))
                {
                    ResetTrail();
                }
            }
        }

        [SerializeField]
        private bool m_Emitting = true;
        /// <summary>
        /// 是否画Trail,这个变量会将UITrailRenderer的开销降到最低。
        /// </summary>
        public bool emitting
        {
            get { return m_Emitting; }
            set
            {
                if (SetPropertyUtility.SetStruct(ref m_Emitting, value))
                {
                    ResetTrail();
                    canvasRenderer.cull = !m_Emitting;
                }
            }
        }

        [SerializeField]
        private UnityEngine.Gradient m_TrailColor = new UnityEngine.Gradient();
        /// <summary>
        /// Trail的颜色渐变
        /// </summary>
        public UnityEngine.Gradient trailColor
        {
            get { return m_TrailColor; }
            set
            {
                SetPropertyUtility.SetClass(ref m_TrailColor, value);
                SetVerticesDirty();
            }
        }

        [SerializeField]
        private AnimationCurve m_Width = new AnimationCurve(new Keyframe{ time = 0.0f, value = 10.0f }, new Keyframe{ time = 1.0f, value = 0.0f });
        /// <summary>
        /// Trail的宽度渐变
        /// </summary>
        public AnimationCurve width
        {
            get { return m_Width; }
            set
            {
                SetPropertyUtility.SetClass(ref m_Width, value);
                SetVerticesDirty();
            }
        }

        [SerializeField]
        [Min(1.0f)]
        private float m_MinVertexDistance = 10.0f;
        /// <summary>
        /// 产生新的路径点的最小移动距离（单位:像素）
        /// </summary>
        public float minVertexDistance
        {
            get { return m_MinVertexDistance; }
            set
            {
                value = Mathf.Max(1.0f, value);
                m_MinVertexDistance = value;
            }
        }

        [SerializeField]
        [Min(0)]
        private int m_NumCapVertices = 0;
        /// <summary>
        /// Trail的端点帽子需要用到顶点数量（越多帽子越平滑）
        /// </summary>
        public int numCapVertices
        {
            get { return m_NumCapVertices; }
            set
            {
                value = Mathf.Max(0, value);
                if (SetPropertyUtility.SetStruct(ref m_NumCapVertices, value))
                {
                    RecaculateCapThetas();
                    SetVerticesDirty();
                }
            }
        }

        [SerializeField]
        private Sprite m_Sprite;
        /// <summary>
        /// 用来渲染Trail的精灵
        /// </summary>
        public Sprite sprite
        {
            get { return m_Sprite; }
            set
            {
                if (SetPropertyUtility.SetClass(ref m_Sprite, value))
                {
                    SetAllDirty();
                    TrackSprite();
                }
            }
        }

        /// <summary>
        /// 最终会设置给CanvasRenderer，用于渲染Trail的纹理
        /// </summary>
        public override Texture mainTexture
        {
            get
            {
                if (sprite == null)
                {
                    if (material != null && material.mainTexture != null)
                    {
                        return material.mainTexture;
                    }
                    return s_WhiteTexture;
                }

                return sprite.texture;
            }
        }

        private List<TrackPoint> m_TrackPointList = new List<TrackPoint>(); // 用来描述路径的点列表
        private float m_AccMovingDistance = 0.0f; // 当前累加移动距离
        private Vector3 m_LastPosition; // 上一次合法位置
        private List<KeyValuePair<float, float>> m_CapThetaList = new List<KeyValuePair<float, float>>(); // 生成帽子Mesh需要用到的Theta对应的Sin, Cos值
        private bool m_IsTrackingUITrailRenderer = false; // 是否在跟踪当前UITrailRenderer

        public override void Cull(Rect clipRect, bool validRect)
        {
            canvasRenderer.cull = !m_Emitting;
        }

        protected override void UpdateGeometry()
        {
            s_VertexHelper.Clear();
            if (m_TrackPointList.Count > 1 && m_Emitting)
            {
                GenerateTrailBody();
                GenerateTrailCap();
            }

            s_VertexHelper.FillMesh(workerMesh);
            canvasRenderer.SetMesh(workerMesh);
        }

        protected override void UpdateMaterial()
        {
            base.UpdateMaterial();

            if (sprite == null)
            {
                canvasRenderer.SetAlphaTexture(null);
                return;
            }

            Texture2D alphaTex = sprite.associatedAlphaSplitTexture;
            if (alphaTex != null)
            {
                canvasRenderer.SetAlphaTexture(alphaTex);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            ResetTrail();
            RecaculateCapThetas();
            TrackSprite();
        }

        protected override void OnDisable()
        {
            if (m_IsTrackingUITrailRenderer)
                UnTrackUITrailRenderer(this);

            m_TrackPointList.Clear();
            canvasRenderer.Clear();
            base.OnDisable();
        }

        protected override void OnTransformParentChanged()
        {
            base.OnTransformParentChanged();
            SetVerticesDirty();
        }

        private static void RebuildUITrailRenderer(SpriteAtlas spriteAtlas)
        {
            for (var i = m_TrackedUITrailRendererList.Count - 1; i >= 0; i--)
            {
                var g = m_TrackedUITrailRendererList[i];
                if (spriteAtlas.CanBindTo(g.sprite))
                {
                    g.SetAllDirty();
                    m_TrackedUITrailRendererList.RemoveAt(i);
                }
            }
        }

        private static void TrackUITrailRenderer(UITrailRenderer g)
        {
            if (!s_Initialized)
            {
                SpriteAtlasManager.atlasRegistered += RebuildUITrailRenderer;
                s_Initialized = true;
            }

            m_TrackedUITrailRendererList.Add(g);
        }

        private static void UnTrackUITrailRenderer(UITrailRenderer g)
        {
            m_TrackedUITrailRendererList.Remove(g);
        }

        private void TrackSprite()
        {
            if (sprite != null && sprite.texture == null)
            {
                TrackUITrailRenderer(this);
                m_IsTrackingUITrailRenderer = true;
            }
        }

        private void RecaculateCapThetas()
        {
            m_CapThetaList.Clear();

            if (m_NumCapVertices <= 0)
                return;

            var thetaStep = Mathf.PI / (m_NumCapVertices + 1);
            var theta = thetaStep;
            for (int i = 0; i < m_NumCapVertices; ++i)
            {
                var cosTheta = Mathf.Cos(theta);
                var sinTheta = Mathf.Sqrt(1.0f - cosTheta * cosTheta);
                m_CapThetaList.Add(new KeyValuePair<float, float>(sinTheta, cosTheta));
                theta += thetaStep;
            }
        }

        private void GenerateTrailCap()
        {
            if (m_NumCapVertices <= 0 || s_VertexHelper.currentVertCount < 4)
                return;

            UIVertex v0 = new UIVertex(), v1 = new UIVertex(), v2 = new UIVertex(), v3 = new UIVertex();
            s_VertexHelper.PopulateUIVertex(ref v0, 0);
            s_VertexHelper.PopulateUIVertex(ref v1, 1);
            s_VertexHelper.PopulateUIVertex(ref v2, 2);
            s_VertexHelper.PopulateUIVertex(ref v3, 3);

            Vector3 posStart = (v0.position + v1.position) * 0.5f;
            Color32 col = ((Color)v0.color + (Color)v1.color) * 0.5f;
            var right = v0.position - posStart;
            var radius = right.magnitude;
            var forward = (posStart - (v2.position + v3.position) * 0.5f).normalized * radius;
            var idxStart = s_VertexHelper.currentVertCount;

            s_VertexHelper.AddVert(posStart, col, (v0.uv0 + v1.uv0) * 0.5f);
            for (int i = 0; i < m_NumCapVertices; ++i)
            {
                var theta = m_CapThetaList[i];
                var projRight = theta.Value * right;
                var projForward = theta.Key * forward;
                var pos = posStart + projRight + projForward;
                var uv = new Vector2((1.0f - theta.Key) * v0.uv0.x, (1.0f - theta.Value) * 0.5f);
                s_VertexHelper.AddVert(pos, col, uv);
            }

            s_VertexHelper.AddTriangle(idxStart, 0, idxStart + 1);
            for (int i = 1; i < m_NumCapVertices; ++i)
            {
                s_VertexHelper.AddTriangle(idxStart, idxStart + i, idxStart + i + 1);
            }
            s_VertexHelper.AddTriangle(idxStart, idxStart + m_NumCapVertices, 1);
        }

        private void GenerateTrailBody()
        {
            var trackCount = m_TrackPointList.Count;
            var indexCount = trackCount * 2;
            for (int i = 0; i < indexCount - 2; i += 2)
            {
                s_VertexHelper.AddTriangle(i, i + 1, i + 2);
                s_VertexHelper.AddTriangle(i + 2, i + 1, i + 3);
            }

            var world2Local = rectTransform.worldToLocalMatrix;
            var tStep = 1.0f / trackCount;
            var t = tStep;
            Vector2 forward = Vector2.zero;
            Vector2 right = Vector2.zero;
            Color32 col = Color.white;
            Vector3 offset;
            for (int i = 0; i < trackCount - 1; ++i)
            {
                forward = (m_TrackPointList[i + 1].Position - m_TrackPointList[i].Position).normalized;
                right = new Vector2(forward.y, -forward.x);
                col = m_TrailColor.Evaluate(t);
                offset = right * m_Width.Evaluate(t);

                s_VertexHelper.AddVert(world2Local.MultiplyPoint(m_TrackPointList[i].Position + offset), col, new Vector2(t, 0.0f));
                s_VertexHelper.AddVert(world2Local.MultiplyPoint(m_TrackPointList[i].Position - offset), col, new Vector2(t, 1.0f));
                t += tStep;
            }

            // The last two points
            offset = right * m_Width.Evaluate(1.0f);
            col = m_TrailColor.Evaluate(1.0f);
            s_VertexHelper.AddVert(world2Local.MultiplyPoint(m_TrackPointList[trackCount - 1].Position + offset), col, new Vector2(1.0f, 0.0f));
            s_VertexHelper.AddVert(world2Local.MultiplyPoint(m_TrackPointList[trackCount - 1].Position - offset), col, new Vector2(1.0f, 1.0f));
        }

        private void ResetTrail()
        {
            m_LastPosition = rectTransform.position;
            m_AccMovingDistance = 0.0f;
            m_TrackPointList.Clear();
            SetVerticesDirty();
        }

        private void Update()
        {
            if (!m_Emitting)
                return;

            var nowTime = Time.time;
            var nowPos = rectTransform.position;
            var sqrDist = Vector2.SqrMagnitude(nowPos - m_LastPosition);

            if (sqrDist > 0.0f)
            {
                SetVerticesDirty();
                var dist = Mathf.Sqrt(sqrDist);
                m_AccMovingDistance += dist;
                if (m_AccMovingDistance >= m_MinVertexDistance)
                {
                    m_TrackPointList.Insert(0, (new TrackPoint { ExpireTime = nowTime + m_Time, Position = nowPos }));
                    m_AccMovingDistance = 0.0f;
                    /*
                    var forward = (nowPos - m_LastPosition) / dist;
                    var accDist = m_MinVertexDistance;
                    while (m_AccMovingDistance >= m_MinVertexDistance)
                    {
                        nowPos = m_LastPosition + accDist * forward;
                        m_TrackPointList.Insert(0, (new TrackPoint { ExpireTime = nowTime + m_Time, Position = nowPos }));
                        m_AccMovingDistance -= m_MinVertexDistance;
                        accDist += m_MinVertexDistance;
                    }
                    */
                }
                m_LastPosition = nowPos;
            }

            while (m_TrackPointList.Count > 0 && nowTime >= m_TrackPointList[m_TrackPointList.Count - 1].ExpireTime)
            {
                m_TrackPointList.RemoveAt(m_TrackPointList.Count - 1);
                SetVerticesDirty();
            }
        }

#if UNITY_EDITOR
        protected override void Reset()
        {
            base.Reset();
            ResetTrail();
        }
#endif
    }
}
