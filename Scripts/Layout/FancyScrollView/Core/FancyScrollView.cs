﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(RectTransform))]
    public abstract class FancyScrollView<TItemData, TContext> : UIBehaviour, ICanvasElement where TContext : class, new()
    {
        [SerializeField, Range(1e-2f, 1f)]
        protected float m_CellInterval = 0.2f;
        /// <summary>
        /// 每个Cell在Viewport中占据的区域大小（Viewport大小为1）
        /// </summary>
        public float cellInterval
        {
            get { return m_CellInterval; }
            set { value = Mathf.Clamp(value, 1e-2f, 1.0f); if (SetPropertyUtility.SetStruct(ref m_CellInterval, value)) { SetDirty(); } }
        }

        [SerializeField, Range(0f, 1f)]
        protected float m_ScrollOffset = 0.5f;
        /// <summary>
        /// 第一个Cell在Viewport中的偏移距离（Viewport大小为1）
        /// </summary>
        public float scrollOffset
        {
            get { return m_ScrollOffset; }
            set { value = Mathf.Clamp(value, 0.0f, 1.0f); if (SetPropertyUtility.SetStruct(ref m_ScrollOffset, value)) { SetDirty(); } }
        }

        /// <summary>
        /// 是否循环显示列表内容
        /// </summary>
        [SerializeField]
        protected bool m_Loop = false;
        public virtual bool loop
        {
            get { return m_Loop; }
            set { if (SetPropertyUtility.SetStruct(ref m_Loop, value)) { SetDirty(); } }
        }

        /// <summary>
        /// 所有Cell的根节点
        /// </summary>
        [SerializeField]
        private Transform m_CellContainer = default;

        protected float m_CurrentScrollerPosition = 0.0f;
        /// <summary>
        /// 当前Scroller返回的位置
        /// </summary>
        public float currentScrollerPosition => m_CurrentScrollerPosition;

        /// <summary>
        /// Cell的预制件
        /// </summary>
        protected abstract GameObject m_CellPrefab { get; }

        protected IList<TItemData> m_ItemsSource { get; set; } = new List<TItemData>();
        /// <summary>
        /// 所有的数据，需要显示的数据会用来初始化对应的Cell
        /// </summary>
        public IList<TItemData> dataList => m_ItemsSource;

        protected TContext m_Context = new TContext();
        /// <summary>
        /// 交互上下文，Cell和SV原则上只通过这个对象进行交互
        /// </summary>
        public TContext context => m_Context;

        /// <summary>
        /// 所有的Cell
        /// </summary>
        private readonly IList<FancyScrollViewCell<TItemData, TContext>> m_Pool = new List<FancyScrollViewCell<TItemData, TContext>>();

        /// <summary>
        /// 标识是否需要内部驱动SV的位置更新一次
        /// </summary>
        protected bool m_ShouldInternalUpdatePosition = false;

        /// <summary>
        /// 标识是否需要内部驱动SV的内容更新一次（同时位置也会更新）
        /// </summary>
        protected bool m_ShouldInternalUpdateContent = false;
        
        /// <summary>
        /// 更新SV的内容
        /// </summary>
        protected virtual void UpdateContents(IList<TItemData> itemsSource)
        {
            m_ItemsSource = itemsSource;
            m_ShouldInternalUpdateContent = true;
            SetDirty();
        }

        /// <summary>
        /// 刷新SV的显示
        /// </summary>
        protected virtual void Refresh()
        {
            Rebuild(CanvasUpdate.PreRender);
            m_ShouldInternalUpdatePosition = false;
            m_ShouldInternalUpdateContent = false;
            UpdatePosition(m_CurrentScrollerPosition, true);
        }

        /// <summary>
        /// 根据Scroller传入的位置信息，刷新SV的显示
        /// </summary>
        protected virtual void UpdatePosition(float position) => UpdatePosition(position, false);

        /// <summary>
        /// 把Scroller传入的位置信息映射到SV空间
        /// </summary>
        protected virtual float ScrollerPositionToScrollView(float posFromScroller)
        {
            return posFromScroller - m_ScrollOffset / m_CellInterval;
        }

        /// <summary>
        /// 当前SV是否处于活跃状态
        /// </summary>
        public override bool IsActive()
        {
            return base.IsActive() && m_CellPrefab != null && m_CellContainer != null;
        }

        /// <summary>
        /// 根据Scroller传入的位置信息，刷新SV的显示
        /// </summary>
        private void UpdatePosition(float position, bool forceRefresh)
        {
            if (!IsActive())
                return;

            m_CurrentScrollerPosition = position;

            var p = ScrollerPositionToScrollView(position);
            var firstIndex = Mathf.FloorToInt(p);

            var firstPosition = (firstIndex - p) * m_CellInterval;
            if (firstPosition + m_Pool.Count * m_CellInterval < 1f)
            {
                ResizePool(firstPosition);
            }

            UpdateCells(firstPosition, firstIndex, forceRefresh);
        }

        /// <summary>
        /// 调整Pool的大小，保证所有Cell占据的区域能够覆盖Viewport
        /// </summary>
        private void ResizePool(float firstPosition)
        {
            var addCount = Mathf.CeilToInt((1f - firstPosition) / m_CellInterval) - m_Pool.Count;
            for (var i = 0; i < addCount; i++)
            {
                var cell = Instantiate(m_CellPrefab, m_CellContainer)
                    .GetComponent<FancyScrollViewCell<TItemData, TContext>>();
                if (cell == null)
                {
                    throw new MissingComponentException(
                        $"FancyScrollViewCell<{typeof(TItemData).FullName}, {typeof(TContext).FullName}> " +
                        $"component not found in {m_CellPrefab.name}.");
                }

                cell.SetupContext(m_Context);
                cell.SetVisible(false);
                m_Pool.Add(cell);
            }
        }

        /// <summary>
        /// 根据传入的第一个Cell的位置和索引，更新所有的Cell
        /// </summary>
        private void UpdateCells(float firstPosition, int firstIndex, bool forceRefresh)
        {
            var poolCount = m_Pool.Count;
            var dataCount = m_ItemsSource.Count;
            for (var i = 0; i < poolCount; i++)
            {
                var index = firstIndex + i;
                var position = firstPosition + i * m_CellInterval;
                var cell = m_Pool[CircularIndex(index, poolCount)];

                if (m_Loop)
                {
                    index = CircularIndex(index, dataCount);
                }

                // 没有内容可展示或者超出Viewport范围的Cell可以直接隐藏了
                if (index < 0 || index >= dataCount || position > 1f)
                {
                    cell.SetVisible(false);
                    continue;
                }

                if (forceRefresh || cell.index != index || !cell.isVisible)
                {
                    cell.index = index;
                    cell.SetVisible(true);
                    cell.UpdateContent(m_ItemsSource[index]);
                }

                cell.UpdatePosition(position);
            }
        }

        protected int CircularIndex(int i, int size) => size < 1 ? 0 : i < 0 ? size - 1 + (i + 1) % size : i % size;

        /// <summary>
        /// 当SV的属性发生改变时，调用该函数，会在下一帧绘制Canvas前同步SV的状态。
        /// </summary>
        protected void SetDirty()
        {
            if (!IsActive())
                return;

            CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
        }

        /// <summary>
        /// 所有状态同步完毕后，强制刷新一次位置，保证显示的一致性
        /// </summary>
        public void GraphicUpdateComplete()
        {
            m_ShouldInternalUpdatePosition = true;
        }

        public void LayoutComplete() { }
        /// <summary>
        /// 所有因为属性发生改变而需要同步的状态，在这里处理
        /// </summary>
        public virtual void Rebuild(CanvasUpdate executing) { }

        /// <summary>
        /// 自驱动表现刷新
        /// </summary>
        protected virtual void Update()
        {
            if (m_ShouldInternalUpdatePosition)
            {
                UpdatePosition(m_CurrentScrollerPosition, m_ShouldInternalUpdateContent);
                m_ShouldInternalUpdatePosition = false;
                m_ShouldInternalUpdateContent = false;
            }
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            if (Application.isPlaying)
                SetDirty();
        }
#endif
    }

    public sealed class FancyScrollViewNullContext { }

    public abstract class FancyScrollView<TItemData> : FancyScrollView<TItemData, FancyScrollViewNullContext> { }
}