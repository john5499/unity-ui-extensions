﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

namespace UnityEngine.UI.Extensions
{
    public abstract class FancyScrollViewCell<TItemData, TContext> : MonoBehaviour where TContext : class, new()
    {
        /// <summary>
        /// 当前Cell使用的数据的索引，不要对这个属性赋值，它是由该Cell对应的SV来赋值的
        /// </summary>
        public int index { get; set; } = -1;

        /// <summary>
        /// 当前Cell是否可见
        /// </summary>
        public virtual bool isVisible => gameObject.activeSelf;

        /// <summary>
        /// Cell和SV交互的上下文
        /// </summary>
        protected TContext m_Context { get; private set; }

        /// <summary>
        /// 设置Cell和对应SV交互的上下文
        /// </summary>
        /// <param name="context"></param>
        public virtual void SetupContext(TContext context) => m_Context = context;

        /// <summary>
        /// 设置Cell的显隐
        /// </summary>
        public virtual void SetVisible(bool visible) => gameObject.SetActive(visible);

        /// <summary>
        /// 更新Cell的内容
        /// </summary>
        /// <param name="itemData">该Cell当前应该使用更新的数据</param>
        public abstract void UpdateContent(TItemData itemData);

        /// <summary>
        /// 更新Cell的位置，传入的参数是一个归一化的位置值。标示该Cell应该在Viewport的什么地方。
        ///         Viewport
        /// 0------------------------1
        /// </summary>
        /// <param name="position">当前Cell在Viewport中的归一化位置</param>
        public abstract void UpdatePosition(float position);
    }

    public abstract class FancyScrollViewCell<TItemData> : FancyScrollViewCell<TItemData, FancyScrollViewNullContext>
    {
        /// <inheritdoc/>
        public sealed override void SetupContext(FancyScrollViewNullContext context) => base.SetupContext(context);
    }
}