﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions
{
    public abstract class FancyGridView<TItemData, TContext> : FancyScrollRect<TItemData[], TContext>
        where TContext : class, IFancyScrollRectContext, IFancyGridViewContext, new()
    {
        [SerializeField]
        protected float m_ColumnSpacing = 0f;
        /// <summary>
        /// 每列之间的间隔
        /// </summary>
        public float columnSpacing
        {
            get => m_ColumnSpacing;
            set { if (SetPropertyUtility.SetStruct(ref m_ColumnSpacing, value)) { SetDirty(); } }
        }

        [SerializeField, Min(1)]
        protected int m_ColumnCount = 4;
        /// <summary>
        /// 列的数量
        /// </summary>
        public int columnCount
        {
            get => m_ColumnCount;
            set
            {
                m_ColumnCount = Mathf.Max(1, value);
                if (SetPropertyUtility.SetStruct(ref m_ColumnCount, value))
                    SetDirty();
            }
        }

        [SerializeField]
        private GameObject m_Cell = default;
        public GameObject cellTemplate => m_Cell;

        [SerializeField]
        private GameObject m_RowTemplate = default;
        public GameObject rowTemplate => m_RowTemplate;

        protected override GameObject m_CellPrefab => m_RowTemplate;

        private int m_DataCount;
        /// <summary>
        /// 数据的个数
        /// </summary>
        public int dataCount => m_DataCount;

        public override bool IsActive()
        {
            return base.IsActive() && m_RowTemplate != null;
        }

        /// <inheritdoc/>
        public override void Initialize()
        {
            base.Initialize();

            m_Context.CellTemplate = m_Cell;
            m_Context.ScrollDirection = scroller.scrollDirection;
            m_Context.GetColumnCount = () => m_ColumnCount;
            m_Context.GetColumnSpacing = () => m_ColumnSpacing;
        }

        public virtual void UpdateContents(IList<TItemData> items)
        {
            m_DataCount = items.Count;

            var rows = items
                .Select((item, index) => (item, index))
                .GroupBy(x => x.index / m_ColumnCount, x => x.item)
                .Select(group => group.ToArray())
                .ToArray();

            UpdateContents(rows);
        }

        public override void ScrollTo(int itemIndex, float duration, Alignment alignment = Alignment.Center, Action onComplete = null)
        {
            var rowIndex = itemIndex / m_ColumnCount;
            base.ScrollTo(rowIndex, duration, alignment, onComplete);
        }

        public override void ScrollTo(int itemIndex, float duration, Ease easing, Alignment alignment = Alignment.Center, Action onComplete = null)
        {
            var rowIndex = itemIndex / m_ColumnCount;
            base.ScrollTo(rowIndex, duration, easing, alignment, onComplete);
        }

        public virtual void JumpTo(int itemIndex, Alignment alignment = Alignment.Center)
        {
            var rowIndex = itemIndex / m_ColumnCount;
            scroller.currentPosition = ScorllViewToScrollerPosition(rowIndex, alignment);
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            m_ColumnCount = Mathf.Max(1, m_ColumnCount);
            base.OnValidate();
        }
#endif
    }
}