﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

using System.Linq;

namespace UnityEngine.UI.Extensions
{
    public abstract class FancyGridViewRow<TItemData, TContext> : FancyScrollRectCell<TItemData[], TContext>
        where TContext : class, IFancyScrollRectContext, IFancyGridViewContext, new()
    {
        protected virtual FancyScrollViewCell<TItemData, TContext>[] Cells { get; private set; }

        protected virtual FancyScrollViewCell<TItemData, TContext>[] InstantiateCells()
        {
            return Enumerable.Range(0, m_Context.GetColumnCount())
                .Select(_ => Instantiate(m_Context.CellTemplate, transform))
                .Select(x => x.GetComponent<FancyScrollViewCell<TItemData, TContext>>())
                .ToArray();
        }

        /// <inheritdoc/>
        public override void SetupContext(TContext context)
        {
            base.SetupContext(context);

            Cells = InstantiateCells();
            Debug.Assert(Cells.Length == m_Context.GetColumnCount());

            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i].SetupContext(context);
            }
        }

        /// <inheritdoc/>
        public override void UpdateContent(TItemData[] rowContents)
        {
            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i].index = i + index * m_Context.GetColumnCount();
                Cells[i].SetVisible(i < rowContents.Length);

                if (Cells[i].isVisible)
                {
                    Cells[i].UpdateContent(rowContents[i]);
                }
            }
        }

        /// <inheritdoc/>
        public override void UpdatePosition(float position)
        {
            base.UpdatePosition(position);

            for (var i = 0; i < Cells.Length; i++)
            {
                Cells[i].UpdatePosition(position);
            }
        }

        /*
        /// <inheritdoc/>
        protected override void UpdatePosition(float position)
        {
            base.UpdatePosition(position);
            transform.localPosition = Context.ScrollDirection == ScrollDirection.Horizontal
                ? new Vector2(viewportPosition, transform.localPosition.y)
                : new Vector2(transform.localPosition.x, viewportPosition);
        }
        */
    }
}