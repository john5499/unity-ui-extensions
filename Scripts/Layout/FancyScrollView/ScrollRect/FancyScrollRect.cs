﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

using System;
using System.Collections.Generic;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(Scroller))]
    public abstract class FancyScrollRect<TItemData, TContext> : FancyScrollView<TItemData, TContext>
        where TContext : class, IFancyScrollRectContext, new()
    {
        [SerializeField]
        protected float m_PaddingHead = 0.0f;
        /// <summary>
        /// SV头部留白，单位（像素）
        /// </summary>
        public float paddingHead
        {
            get => m_PaddingHead;
            set { value = Mathf.Max(0.0f, value); if (SetPropertyUtility.SetStruct(ref m_PaddingHead, value)) { SetDirty(); } }
        }

        [SerializeField]
        protected float m_PaddingTail = 0.0f;
        /// <summary>
        /// SV尾部留白，单位（像素）
        /// </summary>
        public float paddingTail
        {
            get => m_PaddingTail;
            set { value = Mathf.Max(0.0f, value); if (SetPropertyUtility.SetStruct(ref m_PaddingTail, value)) { SetDirty(); } }
        }

        /// <summary>
        /// Cell之间的间隔
        /// </summary>
        [SerializeField]
        protected float m_Spacing = 0f;
        public float spacing
        {
            get => m_Spacing;
            set { value = Mathf.Max(0.0f, value); if (SetPropertyUtility.SetStruct(ref m_Spacing, value)) { SetDirty(); } }
        }

        /// <inheritdoc/>
        public override bool loop
        {
            set
            {
                base.loop = value;
                if (m_Loop)
                {
                    m_PaddingHead = 0.0f;
                    m_PaddingTail = m_Spacing;
                }
            }
        }

        private Scroller m_CachedScroller;
        /// <summary>
        /// 该SV关联的Scorller
        /// </summary>
        protected Scroller scroller => m_CachedScroller ?? (m_CachedScroller = GetComponent<Scroller>());

        /// <summary>
        /// 每个Cell的尺寸，单位（px）
        /// </summary>
        protected float m_CellSize = 0.0f;
        /// <summary>
        /// 是否可以滑动
        /// </summary>
        protected virtual bool scrollable => m_ContentLength > 0f;
        /// <summary>
        /// SV头部留白，单位（Cell)
        /// </summary>
        private float m_PaddingHeadLength = 0.0f;
        /// <summary>
        /// SV尾部留白，单位（Cell）
        /// </summary>
        private float m_PaddingTailLength = 0.0f;
        /// <summary>
        /// SV视口区域的长度，单位（Cell）
        /// </summary>
        private float m_ViewportLength = 0.0f;
        /// <summary>
        /// SV内容的长度，单位（Cell）
        /// </summary>
        private float m_ContentLength = 0.0f;

        /// <summary>
        /// 当SV的属性发生改变时，应该调用该函数来更新所有状态
        /// </summary>
        /// <param name="executing"></param>
        public override void Rebuild(CanvasUpdate executing)
        {
            if (executing == CanvasUpdate.PreRender)
            {
                var cellRectTransform = m_CellPrefab.transform as RectTransform;
                m_CellSize = m_Spacing + (scroller.scrollDirection == ScrollDirection.Horizontal ?
                    cellRectTransform.rect.width : cellRectTransform.rect.height);
                m_PaddingHeadLength = m_PaddingHead / m_CellSize;
                m_PaddingTailLength = (m_PaddingTail - m_Spacing) / m_CellSize;

                var vpSize = scroller.viewportSize;
                m_CellInterval = m_CellSize / vpSize;
                m_ScrollOffset = 0.0f;

                m_ViewportLength = vpSize / m_CellSize;
                if (!m_Loop)
                    m_ContentLength = m_ItemsSource.Count + m_PaddingTailLength + m_PaddingHeadLength - m_ViewportLength;
                else
                    m_ContentLength = scroller.maxPosition;
            }
            base.Rebuild(executing);
        }

        public override bool IsActive()
        {
            return base.IsActive() && scroller != null && m_Context.ComputeCellPosition != null;
        }

        /// <summary>
        /// 更新Scroller的属性，当SV的属性发生改变时被调用，保持状态一致
        /// </summary>
        protected void UpdateScroller()
        {
            scroller.draggable = scrollable;
            scroller.scrollSensitivity = ScorllViewToScrollerPosition(m_ViewportLength - m_PaddingHeadLength);
            scroller.currentPosition = m_CurrentScrollerPosition;

            if (scroller.scrollbar)
            {
                var visible = scrollable && scroller.movementType != MovementType.Unrestricted;
                scroller.scrollbar.gameObject.SetActive(visible);
                if (visible) UpdateScrollbarSize(m_ViewportLength);
            }
        }

        private void ShrinkScrollbar(float offset)
        {
            var scale = 1f - (ScrollerPositionToScrollView(offset) + m_PaddingHeadLength) / m_ViewportLength;
            UpdateScrollbarSize(m_ViewportLength * scale);
        }

        /// <summary>
        /// 计算ScrollerBar中滑条的长度
        /// </summary>
        /// <param name="viewportLength"></param>
        protected void UpdateScrollbarSize(float viewportLength)
        {
            var size = m_ContentLength == 0 ? 1.0f : Mathf.Clamp01(viewportLength / m_ContentLength);
            scroller.scrollbar.size = size;
        }

        /// <summary>
        /// 更新SV位置，该函数只通过Scroller来触发，作为回调注册给Scroller
        /// </summary>
        protected override void UpdatePosition(float p)
        {
            if (!IsActive())
                return;

            base.UpdatePosition(p);

            if (scroller.scrollbar && scroller.scrollbar.isActiveAndEnabled)
            {
                if (p > scroller.maxPosition)
                {
                    ShrinkScrollbar(p - scroller.maxPosition);
                }
                else if (p < 0f)
                {
                    ShrinkScrollbar(-p);
                }
            }
        }

        /// <inheritdoc/>
        protected override void UpdateContents(IList<TItemData> items)
        {
            scroller.maxPosition = items.Count - 1;
            base.UpdateContents(items);
        }

        /// <summary>
        /// 初始化函数，在使用前必须调用该方法进行初始化
        /// </summary>
        public virtual void Initialize()
        {
            m_Loop = false;
            m_Context.ComputeCellPosition = ComputeCellPosition;
            scroller.onValueChanged += UpdatePosition;
            scroller.snapEnabled = false;
            scroller.movementType = MovementType.Elastic;
            // 立即执行一次Rebuild，同步状态
            Rebuild(CanvasUpdate.PreRender);
        }

        /// <summary>
        /// 滑动到指定位置
        /// </summary>
        public virtual void ScrollTo(int index, float duration, Alignment alignment = Alignment.Center, Action onComplete = null)
        {
            scroller.ScrollTo(ScorllViewToScrollerPosition(index, alignment), duration, onComplete);
        }

        /// <summary>
        /// 滑动到指定位置
        /// </summary>
        public virtual void ScrollTo(int index, float duration, Ease easing, Alignment alignment = Alignment.Center, Action onComplete = null)
        {
            scroller.ScrollTo(ScorllViewToScrollerPosition(index, alignment), duration, easing, onComplete);
        }

        /// <summary>
        /// Position转换，Scroller到SV
        /// </summary>
        protected override float ScrollerPositionToScrollView(float posFromScroller)
        {
            var pos = 0.0f;
            if (scrollable)
            {
                var nPos = scroller.maxPosition == 0.0f ? 0.0f : posFromScroller / scroller.maxPosition;
                pos = nPos * m_ContentLength - m_PaddingHeadLength;
            }

            // Debug.Log($"ScrollerPosToSV: posFromScroller:{posFromScroller}, posSV:{pos}, contentLength: {m_ContentLength}, paddingHeadLength:{m_PaddingHeadLength}");

            return pos;
        }

        /// <summary>
        /// Position转换，SV到Scroller
        /// </summary>
        protected float ScorllViewToScrollerPosition(float position)
        {
            var pos = 0.0f;
            if (scrollable)
            {
                var nPos = (position + m_PaddingHeadLength) / m_ContentLength;
                pos = nPos * scroller.maxPosition;
            }
            // Debug.Log($"ScrollViewToScrollerPosition posSV:{position}, posScroller:{pos}");
            return pos;
        }

        /// <summary>
        /// Position转换，SV到Scroller，将对齐的锚点也计算在内
        /// </summary>
        protected float ScorllViewToScrollerPosition(float position, Alignment alignment = Alignment.Center)
        {
            var offset = (m_ViewportLength - 1) * GetAnchor(alignment);
            // Debug.Log($"ScrollViewToScrollerPosition posSV:{position}, offset:{offset}, alignment:{alignment}");
            return ScorllViewToScrollerPosition(position - offset);
        }

        protected Vector2 ComputeCellPosition(RectTransform cellRect, float normalizedPos)
        {
            var cellSize = cellRect.rect.size;
            var viewPortSize = scroller.viewport.rect.size;
            Vector2 nPos = scroller.scrollDirection == ScrollDirection.Horizontal ? new Vector2(normalizedPos - 0.5f, 0.0f) : new Vector2(0.0f, 0.5f - normalizedPos);
            Vector2 lPos = nPos * viewPortSize;
            Vector2 offset = cellRect.pivot * cellSize;
            if (scroller.scrollDirection == ScrollDirection.Horizontal)
                offset.y = 0.0f;
            else
            {
                offset.x = 0.0f;
                offset.y = -offset.y;
            }
            return lPos + offset;
        }

        /// <summary>
        /// 计算锚点的Offset
        /// </summary>
        private float GetAnchor(Alignment alignment)
        {
            return (float)alignment * 0.5f;
        }

        /// <summary>
        /// 自驱动表现刷新
        /// </summary>
        protected override void Update()
        {
            if (m_ShouldInternalUpdatePosition)
            {
                UpdateScroller();
            }

            base.Update();
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            if (m_Loop)
            {
                m_PaddingHead = 0.0f;
                m_PaddingTail = m_Spacing;
            }
            base.OnValidate();
        }
    }
#endif

    public abstract class FancyScrollRect<TItemData> : FancyScrollRect<TItemData, FancyScrollRectContext> { }
}