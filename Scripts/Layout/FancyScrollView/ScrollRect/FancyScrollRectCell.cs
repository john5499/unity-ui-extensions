﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

namespace UnityEngine.UI.Extensions
{
    public abstract class FancyScrollRectCell<TItemData, TContext> : FancyScrollViewCell<TItemData, TContext>
        where TContext : class, IFancyScrollRectContext, new()
    {
        /// <inheritdoc/>
        public override void UpdatePosition(float position)
        {
            var cellPosition = m_Context.ComputeCellPosition(transform as RectTransform, position);
            (transform as RectTransform).anchoredPosition = cellPosition;
        }
    }

    public abstract class FancyScrollRectCell<TItemData> : FancyScrollRectCell<TItemData, FancyScrollRectContext>
    {
        /// <inheritdoc/>
        public sealed override void SetupContext(FancyScrollRectContext context) => base.SetupContext(context);
    }
}