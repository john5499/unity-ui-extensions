﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

using System;

namespace UnityEngine.UI.Extensions
{
    public interface IFancyScrollRectContext
    {
        Func<RectTransform, float, Vector2> ComputeCellPosition { get; set; }
    }
}