﻿/// Credit setchi (https://github.com/setchi)
/// Sourced from - https://github.com/setchi/FancyScrollView

using System;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions.EasingCore;

namespace UnityEngine.UI.Extensions
{
    public partial class Scroller
    {
        [Serializable]
        private class Snap
        {
            public bool Enable;
            public float VelocityThreshold;
            public float Duration;
            public Ease Easing;
        }

        private static readonly Func<float, float> DefaultEasingFunction = EasingFunction.Get(Ease.OutCubic);

        private class AutoScrollState
        {
            public bool Enable;
            public bool Elastic;
            public float Duration;
            public Func<float, float> EasingFunction;
            public float StartTime;
            public float EndPosition;

            public Action OnComplete;

            public void Reset()
            {
                Enable = false;
                Elastic = false;
                Duration = 0f;
                StartTime = 0f;
                EasingFunction = DefaultEasingFunction;
                EndPosition = 0f;
                OnComplete = null;
            }

            public void Complete()
            {
                OnComplete?.Invoke();
                Reset();
            }
        }
    }

    [RequireComponent(typeof(RectTransform))]
    public partial class Scroller : UIBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField]
        protected RectTransform m_Viewport;
        /// <summary>
        /// Scroller所作用的视窗
        /// </summary>
        public RectTransform viewport
        {
            get
            {
                if (m_Viewport == null)
                    m_Viewport = GetComponent<RectTransform>();
                return m_Viewport;
            }
            set
            {
                if (value == null)
                    return;

                m_Viewport = value;
            }
        }

        /// <summary>
        /// 视窗的宽高（根据Scroller的滑动方向，返回不同的值）
        /// </summary>
        public float viewportSize => m_ScrollDirection == ScrollDirection.Horizontal ? viewport.rect.size.x : viewport.rect.size.y;

        [SerializeField]
        protected ScrollDirection m_ScrollDirection = ScrollDirection.Vertical;
        /// <summary>
        /// Scroller的滑动方向
        /// </summary>
        public ScrollDirection scrollDirection { get => m_ScrollDirection; set => m_ScrollDirection = value; }

        [SerializeField]
        protected MovementType m_MovementType = MovementType.Elastic;
        /// <summary>
        /// Scroller的滑动方式
        /// </summary>
        public MovementType movementType { get => m_MovementType; set => m_MovementType = value; }

        [SerializeField, Min(0.0f)]
        protected float m_Elasticity = 0.1f;
        /// <summary>
        /// Scroller的弹性系数，类似UGUI.ScrollRect
        /// </summary>
        public float elasticity { get => m_Elasticity; set { m_Elasticity = Mathf.Max(0.0f, value); } }

        [SerializeField, Min(0.0f)]
        protected float m_ScrollSensitivity = 1f;
        /// <summary>
        /// Scroller的滑动灵敏度
        /// </summary>
        public float scrollSensitivity { get => m_ScrollSensitivity; set { m_ScrollSensitivity = Mathf.Max(0.0f, value); } }

        [SerializeField]
        private bool m_Inertia = true;
        /// <summary>
        /// 是否使用惯性，类似UGUI.ScrollRect
        /// </summary>
        public bool inertia { get => m_Inertia; set => m_Inertia = value; }

        [SerializeField, Min(0.0f)]
        private float m_DecelerationRate = 0.03f;
        /// <summary>
        /// 当使用惯性时，Scroller滑动速度的衰减系数, 类似UGUI.ScrollRect
        /// </summary>
        public float decelerationRate { get => m_DecelerationRate; set {  m_DecelerationRate = Mathf.Max(0.0f, value); } }

        /// <summary>
        /// Scroller对齐模式所需要用到的状态
        /// </summary>
        [SerializeField]
        private Snap m_Snap = new Snap
        {
            Enable = true,
            VelocityThreshold = 0.5f,
            Duration = 0.3f,
            Easing = Ease.InOutCubic
        };

        /// <summary>
        /// 是否开启对齐模式。
        /// 当开启对齐模式时，不论怎样滑动Scroller，最后Scroller的当前位置都会对齐到一个整数。
        /// 只有开启对齐模式时，才会有SelectionChange事件触发
        /// </summary>
        public bool snapEnabled { get => m_Snap.Enable; set => m_Snap.Enable = value; }

        /// <summary>
        /// Scroller开始对齐的最小速度阈值
        /// </summary>
        public float snapVelocityThreshold { get => m_Snap.VelocityThreshold; set => m_Snap.VelocityThreshold = value; }

        /// <summary>
        /// Scorller对齐所需要的时间
        /// </summary>
        public float snapDuration { get => m_Snap.Duration; set => m_Snap.Duration = value; }

        /// <summary>
        /// Scroller对齐过程位置的变化趋势
        /// </summary>
        public Ease snapEasing { get => m_Snap.Easing; set => m_Snap.Easing = value; }

        [SerializeField]
        private bool m_Draggable = true;
        /// <summary>
        /// 是否允许通过拖拽来滑动Scroller
        /// </summary>
        public bool draggable { get => m_Draggable; set => m_Draggable = value; }

        [SerializeField]
        private Scrollbar m_Scrollbar = default;
        /// <summary>
        /// 附加在Scroller上的Scroll bar
        /// </summary>
        public Scrollbar scrollbar
        {
            get => m_Scrollbar;
            set
            {
                if (m_Scrollbar != null)
                    m_Scrollbar.onValueChanged.RemoveListener(SetNormalizedPosition);

                if (value != null)
                    value.onValueChanged.AddListener(SetNormalizedPosition);

                m_Scrollbar = value;
            }
        }

        private float m_CurrentPosition;
        /// <summary>
        /// Scroller当前滑动到的位置
        /// </summary>
        public float currentPosition
        {
            get => m_CurrentPosition;
            set
            {
                m_AutoScrollState.Reset();
                m_Velocity = 0f;
                m_Dragging = false;

                InternalUpdatePosition(value);
            }
        }

        private float m_MaxPosition = 0.0f;
        private Vector2 m_MaxPosBound = new Vector2(0, 1.0f);
        /// <summary>
        /// Scroller可以滑动到的最大位置
        /// </summary>
        public virtual float maxPosition
        {
            get => m_MaxPosition;
            set
            {
                m_MaxPosition = Mathf.Max(0.0f, value);
                m_MaxPosBound = new Vector2(Mathf.FloorToInt(m_MaxPosition), m_MaxPosition + 1.0f);
            }
        }

        /// <summary>
        /// 当Scroller位置改变时的回调
        /// </summary>
        public event Action<float> onValueChanged;

        /// <summary>
        /// 当Scroller对齐到一个整数位置时的回调
        /// </summary>
        public event Action<int> onSelectionChanged;

        private float m_PrevPosition; // Scroller上一次更新时的位置
        private float m_ScrollStartPosition; // Scroller开始滑动时的位置
        private readonly AutoScrollState m_AutoScrollState = new AutoScrollState(); // Scroller自动滑动时的上下文
        private bool m_Dragging; // Scroller当前是否在被拖拽
        private float m_Velocity; // Scorller当前滑动的速度

        /// <summary>
        /// Scroller滑动到指定位置
        /// </summary>
        /// <param name="position">指定位置</param>
        /// <param name="duration">滑动所需时间</param>
        /// <param name="onComplete">滑动结束时的回调</param>
        public void ScrollTo(float position, float duration, Action onComplete = null) => ScrollTo(position, duration, Ease.OutCubic, onComplete);

        /// <summary>
        /// Scroller滑动到指定位置
        /// </summary>
        /// <param name="position">指定位置</param>
        /// <param name="duration">滑动所需时间</param>
        /// <param name="easing">滑动到指定位置时，滑动位置的变化趋势</param>
        /// <param name="onComplete">滑动结束时的回调</param>
        public void ScrollTo(float position, float duration, Ease easing, Action onComplete = null) => ScrollTo(position, duration, EasingFunction.Get(easing), onComplete);

        /// <summary>
        /// Scroller滑动到指定位置
        /// </summary>
        /// <param name="position">指定位置</param>
        /// <param name="duration">滑动所需时间</param>
        /// <param name="easingFunction">滑动到指定位置时，滑动位置的变化趋势</param>
        /// <param name="onComplete">滑动结束时的回调</param>
        public void ScrollTo(float position, float duration, Func<float, float> easingFunction, Action onComplete = null)
        {
            if (duration <= 0f)
            {
                position = m_CurrentPosition + CalculateMovementAmount(m_CurrentPosition, position);
                if (m_Snap.Enable)
                {
                    position = Mathf.RoundToInt(position);
                    if (m_MovementType != MovementType.Unrestricted)
                        position = Mathf.Clamp(position, 0.0f, m_MaxPosBound.x);
                }
                currentPosition = position;
                onSelectionChanged?.Invoke((int)currentPosition);
                onComplete?.Invoke();
                return;
            }

            m_AutoScrollState.Reset();
            m_AutoScrollState.Enable = true;
            m_AutoScrollState.Duration = duration;
            m_AutoScrollState.EasingFunction = easingFunction ?? DefaultEasingFunction;
            m_AutoScrollState.StartTime = Time.unscaledTime;
            m_AutoScrollState.EndPosition = m_CurrentPosition + CalculateMovementAmount(m_CurrentPosition, position);
            m_AutoScrollState.OnComplete = onComplete;

            m_Velocity = 0f;
            m_ScrollStartPosition = m_CurrentPosition;
        }

        /// <summary>
        /// 获取从指定起始位置到指定终点位置，Scroller的移动方向
        /// </summary>
        public MovementDirection GetMovementDirection(float startPos, float endPos)
        {
            var movementAmount = CalculateMovementAmount(startPos, endPos);
            return m_ScrollDirection == ScrollDirection.Horizontal
                ? movementAmount > 0
                    ? MovementDirection.Left
                    : MovementDirection.Right
                : movementAmount > 0
                    ? MovementDirection.Up
                    : MovementDirection.Down;
        }

        public override bool IsActive()
        {
            return base.IsActive() && m_Viewport != null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (m_Scrollbar != null)
                m_Scrollbar.onValueChanged.AddListener(SetNormalizedPosition);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (m_Scrollbar != null)
                m_Scrollbar.onValueChanged.RemoveListener(SetNormalizedPosition);
        }

        private Vector2 m_BeginDragPointerPosition;
        /// <inheritdoc/>
        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            if (!m_Draggable || eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                viewport,
                eventData.position,
                eventData.pressEventCamera,
                out m_BeginDragPointerPosition);

            m_ScrollStartPosition = m_CurrentPosition;
            m_Dragging = true;
            m_AutoScrollState.Reset();
        }

        /// <inheritdoc/>
        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (!m_Draggable || eventData.button != PointerEventData.InputButton.Left || !m_Dragging)
            {
                return;
            }

            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(
                viewport,
                eventData.position,
                eventData.pressEventCamera,
                out var dragPointerPosition))
            {
                return;
            }

            var vpSize = viewportSize;
            var pointerDelta = dragPointerPosition - m_BeginDragPointerPosition;
            var position = (m_ScrollDirection == ScrollDirection.Horizontal ? -pointerDelta.x : pointerDelta.y)
                           / vpSize
                           * m_ScrollSensitivity
                           + m_ScrollStartPosition;

            var offset = CalculateOffset(position);
            position += offset;

            if (movementType == MovementType.Elastic)
            {
                if (offset != 0f)
                {
                    position -= RubberDelta(offset, vpSize);
                }
            }

            InternalUpdatePosition(position);
        }

        /// <inheritdoc/>
        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            if (!m_Draggable || eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            m_Dragging = false;
        }

        /// <summary>
        /// 计算传入位置到离它最近的合法位置的偏移
        /// </summary>
        float CalculateOffset(float position)
        {
            if (movementType == MovementType.Unrestricted)
                return 0f;

            if (position < 0f)
                return -position;

            if (position > m_MaxPosition)
                return m_MaxPosition - position;

            return 0f;
        }

        /// <summary>
        /// 通过一个归一化的值来设置Scroller位置，
        /// </summary>
        /// <param name="normalizedPos"></param>
        private void SetNormalizedPosition(float normalizedPos)
        {
            if (m_MovementType == MovementType.Unrestricted)
                return;

            InternalUpdatePosition(Mathf.Clamp01(normalizedPos) * m_MaxPosition, false);
        }

        /// <summary>
        /// 更新Scroller的位置
        /// </summary>
        private void InternalUpdatePosition(float position, bool updateScrollbar = true)
        {
            if (!IsActive())
                return;

            onValueChanged?.Invoke(m_CurrentPosition = position);

            if (scrollbar && updateScrollbar)
            {
                if (m_MovementType == MovementType.Unrestricted || m_MaxPosition == 0.0f)
                    scrollbar.value = 1.0f;
                else
                    scrollbar.value = Mathf.Clamp01(position / m_MaxPosition);
            }
        }

        private void Update()
        {
            var deltaTime = Time.unscaledDeltaTime;
            var offset = CalculateOffset(m_CurrentPosition);

            // 这个代码块，用于处理Scroller被AutoScroll驱动时的更新
            if (m_AutoScrollState.Enable)
            {
                var position = 0f;

                // 这块代码只会在Scroller滑动到边界处被执行， 主要是用来表现Scroller的在边界处的反弹效果
                if (m_AutoScrollState.Elastic)
                {
                    position = Mathf.SmoothDamp(m_CurrentPosition, m_CurrentPosition + offset, ref m_Velocity, m_Elasticity, Mathf.Infinity, deltaTime);

                    if (Mathf.Abs(m_Velocity) < 0.01f)
                    {
                        m_Velocity = 0f;
                        m_AutoScrollState.Complete();
                    }
                }
                // 这块代码在Scroller被ScrollTo接口驱动时执行，根据提前计算好的终点位置，来表现Scroller的滑动效果
                else
                {
                    var alpha = Mathf.Clamp01((Time.unscaledTime - m_AutoScrollState.StartTime) /
                                               Mathf.Max(m_AutoScrollState.Duration, float.Epsilon));
                    position = Mathf.LerpUnclamped(m_ScrollStartPosition, m_AutoScrollState.EndPosition,
                        m_AutoScrollState.EasingFunction(alpha));

                    if (Mathf.Approximately(alpha, 1f))
                    {
                        m_AutoScrollState.Complete();
                    }
                }

                var lastFrame = !m_AutoScrollState.Enable;

                // 这块代码只会在AutoScroll的最后一帧执行，用于对齐Scroller到某个整数位置。
                if (lastFrame)
                {
                    if (m_Snap.Enable)
                        position = Mathf.RoundToInt(position);
                    if (m_MovementType != MovementType.Unrestricted)
                        position = Mathf.Clamp(position, 0, m_MaxPosBound.x);
                }

                InternalUpdatePosition(position);

                if (lastFrame && m_Snap.Enable)
                    onSelectionChanged?.Invoke((int)position);
            }
            // 这个代码块，用于处理Scroller被当前速度或者超界驱动时的更新
            else if (!m_Dragging && (!Mathf.Approximately(offset, 0f) || !Mathf.Approximately(m_Velocity, 0f)))
            {
                var position = m_CurrentPosition;

                // 如果Scroller超界，那么从超界区域反弹会正常区域的更新，委托给AutoScroll来完成
                if (movementType == MovementType.Elastic && !Mathf.Approximately(offset, 0f))
                {
                    m_AutoScrollState.Reset();
                    m_AutoScrollState.Enable = true;
                    m_AutoScrollState.Elastic = true;
                }
                // 如果Scroller使用惯性，那么接下来的位置更新，则根据当前Scroller的速度来驱动
                else if (m_Inertia)
                {
                    m_Velocity *= Mathf.Pow(decelerationRate, deltaTime);

                    if (Mathf.Abs(m_Velocity) < 0.001f)
                    {
                        m_Velocity = 0.0f;
                    }

                    position += m_Velocity * deltaTime;

                    if (m_Snap.Enable && Mathf.Abs(m_Velocity) < m_Snap.VelocityThreshold)
                    {
                        ScrollTo(Mathf.RoundToInt(m_CurrentPosition), m_Snap.Duration, m_Snap.Easing);
                    }
                }
                // 如果没有惯性，则不用速度驱动Scroller位置更新
                else
                {
                    m_Velocity = 0f;
                }

                // 这个代码块，只会在有惯性时才会被执行
                if (!Mathf.Approximately(m_Velocity, 0f))
                {
                    if (movementType == MovementType.Clamped)
                    {
                        offset = CalculateOffset(position);
                        position += offset;

                        if (Mathf.Approximately(position, 0f) || Mathf.Approximately(position, m_MaxPosition))
                        {
                            m_Velocity = 0f;
                        }
                    }

                    InternalUpdatePosition(position);
                }
            }
            // 本来，这里应该有一个用于处理Sroller被用户拖动时更新的代码块，被放在了OnDrag里面

            // 当Scroller在拖拽的时候，根据拖拽速度，更新惯性滑动所需要的速度
            if (!m_AutoScrollState.Enable && m_Dragging && m_Inertia)
            {
                var newVelocity = (m_CurrentPosition - m_PrevPosition) / deltaTime;
                m_Velocity = Mathf.Lerp(m_Velocity, newVelocity, deltaTime * 10f);
            }

            m_PrevPosition = m_CurrentPosition;
        }

        private float CalculateMovementAmount(float sourcePosition, float destPosition)
        {
            if (movementType != MovementType.Unrestricted)
                return Mathf.Clamp(destPosition, 0, m_MaxPosition) - sourcePosition;

            var amount = CircularPosition(destPosition) - CircularPosition(sourcePosition);

            if (Mathf.Abs(amount) > m_MaxPosBound.y * 0.5f)
            {
                amount = Mathf.Sign(-amount) * (m_MaxPosBound.y - Mathf.Abs(amount));
            }

            return amount;
        }

        private float CircularPosition(float p) => p < 0 ? m_MaxPosBound.y + p % m_MaxPosBound.y : p % m_MaxPosBound.y;

        private float RubberDelta(float overStretching, float viewSize) =>
            (1 - 1 / (Mathf.Abs(overStretching) * 0.55f / viewSize + 1)) * viewSize * Mathf.Sign(overStretching);
    }
}