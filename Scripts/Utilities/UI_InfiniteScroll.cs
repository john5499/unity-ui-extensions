﻿/// Credit Tomasz Schelenz 
/// Sourced from - https://bitbucket.org/ddreaper/unity-ui-extensions/issues/81/infinite-scrollrect
/// Demo - https://www.youtube.com/watch?v=uVTV7Udx78k  - configures automatically.  - works in both vertical and horizontal (but not both at the same time)  - drag and drop  - can be initialized by code (in case you populate your scrollview content from code)

using System.Collections.Generic;
using System.Collections;

namespace UnityEngine.UI.Extensions
{
    /// <summary>
    /// Infinite scroll view with automatic configuration 
    /// 
    /// 
    /// Notes
    /// - doesn't work in both vertical and horizontal orientation at the same time.
    /// - in order to work it disables layout components and size fitter if present(automatically)
    /// 
    /// </summary>
    [AddComponentMenu("UI/Extensions/UI Infinite Scroll")]
    public class UI_InfiniteScroll : MonoBehaviour
    {
        private ScrollRect m_ScrollRect;
        private ScrollRect.MovementType m_OriginMovmentType;
        private Vector2 m_OriginContentSizeDelta;
        private Rect m_ViewportRect;
        private Matrix4x4 m_World2Viewport;

        private float m_ItemSize = 0;
        private int m_ItemCount = 0;
        private Vector3[] m_ItemCorner = new Vector3[4];
        private Vector2 m_LastContentPosition = Vector2.zero;

        private bool m_Inited = false;

        private void Awake()
        {
            m_ScrollRect = GetComponent<ScrollRect>();
            if (m_ScrollRect == null)
            {
                Debug.LogError("UI_InfiniteScroll => No ScrollRect component found");
            }
        }

        private void OnScroll(Vector2 pos)
        {
            var content = m_ScrollRect.content;
            var posContent = (Vector2)content.localPosition;
            var delta = posContent - m_LastContentPosition;
            m_LastContentPosition = posContent;

            var isHorizontal = m_ScrollRect.horizontal;
            if (Mathf.Approximately(delta.x, 0.0f) && isHorizontal)
                return;
            if (Mathf.Approximately(delta.y, 0.0f) && !isHorizontal)
                return;

            bool scrollToBottom = isHorizontal ? delta.x > 0 : delta.y < 0;
            if (scrollToBottom)
            {
                var item = content.GetChild(m_ItemCount - 1) as RectTransform;
                var itemRect = GetItemRect(item);
                var needMoveToFirst = isHorizontal ? itemRect.min.x > m_ViewportRect.max.x : itemRect.max.y < m_ViewportRect.min.y;
                if (needMoveToFirst)
                {
                    var newAnchoredPosition = item.anchoredPosition;
                    if (isHorizontal)
                        newAnchoredPosition.x -= m_ItemCount * m_ItemSize;
                    else
                        newAnchoredPosition.y += m_ItemCount * m_ItemSize;

                    item.anchoredPosition = newAnchoredPosition;
                    item.SetAsFirstSibling();
                }
            }
            else
            {
                var item = content.GetChild(0) as RectTransform;
                var itemRect = GetItemRect(item);
                var needMoveToLast = isHorizontal ? itemRect.max.x < m_ViewportRect.min.x : itemRect.min.y > m_ViewportRect.max.y;
                if (needMoveToLast)
                {
                    var newAnchoredPosition = item.anchoredPosition;
                    if (isHorizontal)
                        newAnchoredPosition.x += m_ItemCount * m_ItemSize;
                    else
                        newAnchoredPosition.y -= m_ItemCount * m_ItemSize;

                    item.anchoredPosition = newAnchoredPosition;
                    item.SetAsLastSibling();
                }
            }
        }

        private void OnEnable()
        {
            StartCoroutine(DelayInit());
        }

        private void OnDisable()
        {
            DeInit();
        }

        private IEnumerator DelayInit()
        {
            yield return null;
            yield return new WaitForEndOfFrame();
            Init();
        }

        private void DeInit()
        {
            if (m_ScrollRect == null || !m_Inited)
                return;

            m_Inited = false;

            m_ScrollRect.onValueChanged.RemoveListener(OnScroll);
            m_ScrollRect.movementType = m_OriginMovmentType;
            m_ScrollRect.content.sizeDelta = m_OriginContentSizeDelta;
            ToggleContentAutoLayout(true);
        }

        private void Init()
        {
            if (m_ScrollRect == null || m_Inited)
                return;

            m_ViewportRect = m_ScrollRect.viewport.rect;
            m_World2Viewport = m_ScrollRect.viewport.worldToLocalMatrix;
            m_OriginMovmentType = m_ScrollRect.movementType;
            m_OriginContentSizeDelta = m_ScrollRect.content.sizeDelta;
            m_LastContentPosition = m_ScrollRect.content.localPosition;

            if (!UpdateScrollItems())
                return;

            m_ScrollRect.content.sizeDelta = Vector2.zero;
            m_ScrollRect.onValueChanged.AddListener(OnScroll);
            m_ScrollRect.movementType = ScrollRect.MovementType.Unrestricted;

            ToggleContentAutoLayout(false);

            if (m_ScrollRect.horizontal && m_ScrollRect.vertical)
                Debug.LogError("UI_InfiniteScroll doesn't support scrolling in both directions, plase choose one direction (horizontal or vertical)");

            m_Inited = true;
        }

        private void ToggleContentAutoLayout(bool enable)
        {
            var componentList = new List<MonoBehaviour>();
            m_ScrollRect.content.GetComponents(componentList);
            for (int i = 0; i < componentList.Count; ++i)
            {
                var component = componentList[i];
                if (component is ILayoutSelfController || component is ILayoutGroup)
                    component.enabled = enable;
            }
        }

        private bool UpdateScrollItems()
        {
            var itemCount = m_ScrollRect.content.childCount;
            if (itemCount <= 1)
                return false;

            var content = m_ScrollRect.content;

            if (m_ScrollRect.horizontal)
            {
                var ch1 = (content.GetChild(1) as RectTransform).anchoredPosition.x;
                var ch0 = (content.GetChild(0) as RectTransform).anchoredPosition.x;
                m_ItemSize = Mathf.Abs(ch1 - ch0);
                if (m_ItemSize * itemCount <= m_ViewportRect.width)
                    return false;
            }
            else if (m_ScrollRect.vertical)
            {
                var ch1 = (content.GetChild(1) as RectTransform).anchoredPosition.y;
                var ch0 = (content.GetChild(0) as RectTransform).anchoredPosition.y;
                m_ItemSize = Mathf.Abs(ch1 - ch0);
                if (m_ItemSize * itemCount <= m_ViewportRect.height)
                    return false;
            }

            m_ItemCount = itemCount;
            return true;
        }

        private Rect GetItemRect(RectTransform item)
        {
            item.GetWorldCorners(m_ItemCorner);

            var vMin = new Vector2(float.MaxValue, float.MaxValue);
            var vMax = new Vector2(float.MinValue, float.MinValue);
            for (int j = 0; j < 4; j++)
            {
                Vector3 v = m_World2Viewport.MultiplyPoint3x4(m_ItemCorner[j]);
                vMin = Vector2.Min(v, vMin);
                vMax = Vector2.Max(v, vMax);
            }

            return Rect.MinMaxRect(vMin.x, vMin.y, vMax.x, vMax.y);
        }

        public void Reset()
        {
            DeInit();
            Init();
        }
    }
}